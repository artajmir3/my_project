import sys
import argparse

parser = argparse.ArgumentParser()

parser.add_argument('-f', action='store', dest='filename', help='Filename')
parser.add_argument('-v', action='store', dest='value', help='value')

results = parser.parse_args()

file = open(results.filename, 'r')

output = file.read()
if output== str(results.value) + "\n":
    print("ok")
    sys.exit(0)
else:
    print("nokay")
    sys.exit(1)