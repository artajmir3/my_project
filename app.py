import argparse

def add(a, b):
    return int(a) + int(b)

parser = argparse.ArgumentParser()

parser.add_argument('-a', action='store', dest='first_value', help='Store the first value')
parser.add_argument('-b', action='store', dest='second_value', help='Store the second value')

results = parser.parse_args()

print(add(results.first_value, results.second_value))